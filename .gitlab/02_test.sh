#!/bin/sh

#if [ "$1" == "code-quality" ]; then
#  docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
#  docker run --env CODECLIMATE_CODE="$PWD" \
#    --volume "$PWD":/code \
#    --volume /var/run/docker.sock:/var/run/docker.sock \
#    --volume /tmp/cc:/tmp/cc codeclimate/codeclimate analyze -f json > gl-code-quality-report.json
#fi
#
#if [ "$1" == "container-scan" ]; then
#  docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
#  docker run -d --name db arminc/clair-db:latest
#  docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.8_fe9b059d930314b54c78f75afe265955faf4fdc1
#  docker pull $STABLE_TEST_IMAGE
#  wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
#  mv clair-scanner_linux_amd64 clair-scanner
#  chmod +x clair-scanner
#  touch clair-whitelist.yml
#  retries=0
#  echo "Waiting for clair daemon to start"
#  export IP=$(hostname -i)
#  while( ! wget -T 10 -q -O /dev/null http://localhost:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
#  ./clair-scanner -c http://localhost:6060 --ip=$IP -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml $STABLE_TEST_IMAGE || true
#  echo $?
#fi

#if [ "$1" == "unit-test" ]; then
  npm install
  npm test
#fi
