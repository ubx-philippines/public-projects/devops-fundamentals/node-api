#!/bin/sh
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
docker pull $STABLE_TEST_IMAGE
docker tag $STABLE_TEST_IMAGE $STABLE_RELEASE_IMAGE
docker push $STABLE_RELEASE_IMAGE