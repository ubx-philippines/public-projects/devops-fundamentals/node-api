#!/bin/sh
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
docker build -t $STABLE_TEST_IMAGE .
docker push $STABLE_TEST_IMAGE