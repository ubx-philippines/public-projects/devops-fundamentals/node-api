const fullName = require('./name');

test('Returns Louis Concepcion', ()=> {
  expect(fullName('Louis', 'Concepcion')).toBe('Louis Concepcion');
});

test('Returns World', ()=> {
  expect(fullName(undefined, undefined)).toBe('World');
});