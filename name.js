function fullName(firstName, lastName) {

  if (firstName && lastName) {
    return firstName + " " + lastName;
  } else {
    return 'World';
  }

}
module.exports = fullName;