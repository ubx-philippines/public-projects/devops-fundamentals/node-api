FROM node:12.18-alpine

WORKDIR /var/www​​

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

ENTRYPOINT ["node", "app.js"]​
